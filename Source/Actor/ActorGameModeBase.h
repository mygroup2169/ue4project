// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ActorGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ACTOR_API AActorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
